echo on
pushd "C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\VC\Auxiliary\Build"
call vcvarsall.bat %ARCH% -vcvars_ver=%VCVARS_VER%
popd
echo on

set PYTHON_ROOT=c:/Python39_%ARCH%
set PYTHON_VER=39
set BASE=%CD:\=/%
set BOOST_ROOT=%BASE%/boost
set TANGO_ROOT=%BASE%/tango

c:\Python39_%ARCH%\python -m venv .\venv
call .\venv\Scripts\activate.bat
echo on

set VENV_ROOT=%VIRTUAL_ENV:\=/%

pip install nose wheel numpy

curl -L https://gitlab.com/api/v4/projects/35357105/packages/generic/libboost-python/%BOOST_VER%/boost-%BOOST_VER%_%ARCH%_%VC_VER%.zip -o boost.zip && ^
curl -L https://gitlab.com/api/v4/projects/35357105/packages/generic/libtango/%TANGO_VER%/libtango_%TANGO_VER%_%VC_VER%_%ARCH%_static_release.zip -o tango.zip && ^
7z x boost.zip -oboost && ^
7z x tango.zip && ^
ren libtango_%TANGO_VER%_%VC_VER%_%ARCH%_static_release tango || goto :error

pushd pytango
git apply ..\pytango.patch || goto :error
popd

md build
pushd build
cmake %BASE%/pytango -G"Ninja" -DCMAKE_CXX_FLAGS_RELEASE="/MT /O2 /Ob2 /DNDEBUG" -DCMAKE_BUILD_TYPE="Release" && ^
ninja || goto :error
popd

goto :EOF

:error
echo Failed with error #%errorlevel%.
exit /b %errorlevel%
